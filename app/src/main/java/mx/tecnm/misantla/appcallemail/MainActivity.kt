package mx.tecnm.misantla.appcallemail

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Dar permisos necesarios
        //Apartir de android 6 se necesita comprobar los permisos

        val comprobar_permiso = ContextCompat.checkSelfPermission(
            this,android.Manifest.permission.CALL_PHONE)

        if(comprobar_permiso != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this,arrayOf<String>(android.Manifest.permission.CALL_PHONE),225)
        }

        llamada.setOnClickListener {
            val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel: 123456789"))
        //    val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:" + tel.text.toString()))

            startActivity(intent)
        }
    }
}